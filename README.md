# FDF

The Fields Data File format allows to store data under the format of named fields and values.



## Data types

Supported data types are:

| # | Name   | Description             |
|---|--------|-------------------------|
| 0 | int    | Integer                 |
| 1 | float  | Floating-point number   |
| 2 | bool   | Boolean                 |
| 3 | string | Characters sequence     |



## Syntax

Here is an example of an FDF file:

```
# This is a comment
int = 0 # This is another comment
float = 0.1
bool = true
string = "This is a string"
```
