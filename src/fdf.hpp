#ifndef FDF_HPP
# define FDF_HPP

# include<fstream>
# include<iostream>
# include<sstream>
# include<string>
# include<typeinfo>
# include<unordered_map>

namespace FDF
{
	using namespace std;

	/**
	 * An enum containing all supported data types.
	 */
	enum Type : uint8_t
	{
		/**
		 * An integer.
		 */
		INT_T = 0,

		/**
		 * A floating-point number.
		 */
		FLOAT_T = 1,

		/**
		 * A boolean (true or false).
		 */
		BOOL_T = 2,

		/**
		 * A characters sequence.
		 */
		STRING_T = 3,
	};

	class Value
	{
		public:
			/**
			 * \return The value's type
			 */
			inline Type get_type() const
			{
				return type;
			}

			/**
			 * \return The value
			 *
			 * \tparam T	The value's type
			 */
			template<typename T>
			inline operator T() const
			{
				return *reinterpret_cast<T*>(value);
			}

			/**
			 * Assigns the specified value.
			 *
			 * \param v	The value
			 */
			inline void operator=(const int v)
			{
				free();

				type = INT_T;
				value = new int;
				*((int*) value) = v;
			}

			/**
			 * Assigns the specified value.
			 *
			 * \param v	The value
			 */
			inline void operator=(const float v)
			{
				free();

				type = FLOAT_T;
				value = new float;
				*((float*) value) = v;
			}

			/**
			 * Assigns the specified value.
			 *
			 * \param v	The value
			 */
			inline void operator=(const bool v)
			{
				free();

				type = BOOL_T;
				value = new bool;
				*((bool*) value) = v;
			}

			/**
			 * Assigns the specified value.
			 *
			 * \param v	The value
			 */
			inline void operator=(const string v)
			{
				free();

				type = STRING_T;
				value = new string;
				*((string*) value) = v;
			}

			/**
			 * \return A string representation of the value
			 */
			string dump() const;

		private:
			Type type;
			void* value;

			void free();
	};

	class File
	{
		public:
			/**
			 * Parses the given string.
			 *
			 * \param str	The string
			 */
			void operator<<(const string str);

			/**
			 * Parses the given file.
			 *
			 * \param file	An ifstream on the file
			 */
			void operator<<(ifstream file);

			/**
			 * \return An unordered_map containing all the file's fields
			 */
			inline const unordered_map<string, Value>& get_fields() const
			{
				return fields;
			}

			/**
			 * Writes the fields to the given file.
			 *
			 * \param file	An ofstream on tne file
			 */
			void write(ofstream file) const;

		private:
			unordered_map<string, Value> fields;

			void add_field(string& str);
			void put_value(Value& value, const string& str);
	};
}

#endif
